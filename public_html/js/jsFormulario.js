var jsFormulario = function () {
    return {
        init_validacion: function () {

            $("#formulario_prueba").validate({
                errorClass: "validation_error",
                rules: {
                    textNombre: {
                        required: true
                     
                    },
                    textinput: {
                        required: true
                       
                    },
                    textDni: {
                        required: true,
                        maxlength: 8,
                        minlength:8,
                        digits: true
                    },
                    textTel: {
                        required: true,
                        maxlength: 6,
                        minlength:6,
                        digits:true
                    },
                    textMail: {
                        required: true,
                        email:true
                    },
                    radios:{
                      required: true  
                    },
                    textInstitucion: {
                        required: true
                    }
                },
                messages: {
                    textNombre: "Campo Requerido",
                    textinput: "Campo Requerido",
                    textDni: "Campo Requerido",
                    textTel: "Campo Requerido",
                    textMail: "Campo Requerido",
                    radios: "Campo Requerido",
                    textInstitucion: "Campo Requerido"
                },
                highlight: function (element) {
                    $(element).parent().addClass('error')
                    //alert(element);
                },
                unhighlight: function (element) {
                    $(element).parent().removeClass('error')
                }
//                errorPlacement: function (error, element) {
//                    element.before(error);
//                }
            });
        },
    }
}();

$(function () {
    jsFormulario.init_validacion();
});

